// It will allow us to use the contents of the task.js file in the model folder.
const Task = require("../models/task");

// Get all tasks
module.exports.getAllTasks = () =>{
	// The "then" method is used  to wait for the Mongoose "find" method to finish before sending the result back to the route and eventually to the client/postman.
	return Task.find({}).then(result => result);
}

// Create a task
module.exports.createTask = (reqBody) =>{

	// Create a task object based on the mongoose model "Task"
	let newTask = new Task({
		// Sets the "name" property with the value received from Postman.
		name: reqBody.name
	})
	// Using callback function: newUser.save(saveErr, savedTask) =>{})
	// Using .then method: newTask.save().then((task, error) =>{})
	return newTask.save().then((task, error) =>{
		if(error){
			console.log(error)
			return false
		}
		else{
			// Returns the new task object saved in the database back to the postman
			return task
		}
	})
}

// Delete a task
// Business Logic
/*
	1. Look for the task with the corresponding id provided in the URL/route
	2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/
		
module.exports.deleteTask = (taskId) =>{
	return Task.findByIdAndRemove(taskId).then((removedTask, err) =>{
		if(err){
			console.log(err);
			return false
		}
		else{
			return removedTask;
		}
	})
}

// Update a Task
// Business Logic
/*
	1. Get the task with the id using the Mongoose method "findById"
	2. Replace the task's name returned from the database with the "name" property from the request body
	3. Save the task
*/

module.exports.updateTask = (taskId, reqBody) =>{
	// findById is the same as "find({_id":"value"})"
	return Task.findById(taskId).then((result, error) =>{
		if(error){
			console.log(error);
			return false;
		}
		else{
			// we reassign the result name with the request body content.
			result.name = reqBody.name;

			return result.save().then((updatedTaskName, updateErr) =>{
				if(updateErr){
					console.log(saveErr);
					return false;
				}
				else{
					return updatedTaskName;
				}
			})
		}
	})
}		
// ===================
/*
	ACTIVITY s31- Get a specific task
*/

// Get a specific Tasks
module.exports.getSpecificTasks = (taskId) =>{
   
   return Task.findById(taskId).then((result, error) =>{

       if(error){
           console.log(error);
           return false;
       }
       else{
           return result;
       }
           
       })
}


// Update task status to complete
module.exports.updateTaskStatus = (taskId) => {
   return Task.findById(taskId).then((result, error) =>{

       if(error){
           console.log(error);
           return false;
       }
       else{
           result.status = "Complete";

           return result.save().then((updatedTaskStatus, updateErr) =>{

               if(updateErr){
                   console.log(updateErr)
                   return false;
               }
               else{
                   return updatedTaskStatus;
               }
           })
       }
   })
}












